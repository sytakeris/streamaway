name := "stream"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "com.opencsv" % "opencsv" % "5.1"
libraryDependencies += "com.google.guava" % "guava" % "28.2-jre"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % "test"

