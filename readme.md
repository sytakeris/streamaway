# Data Processing App

## Info

This app joins View stream with Click steam. And also joins View stream with Viewable Event steam.
Lastly aggregates the data and calculates statistics for the campaign. For joining streams I tried to
implement sliding window

![Alt text](https://ci.apache.org/projects/flink/flink-docs-release-1.10/fig/sliding-windows.svg)

## Running app

For convenience I commited prebuild jar `app.jar`

* Go to the folder with view, viewable events and clicks csv files
* run command jar with arguments example `java -jar app.jar Views.csv Clicks.csv ViewableViewEvents.csv `

This will produce three files `ViewableViews.csv` `ViewsWithClicks.csv` `statistics.csv`

## ViewsWithClicks.csv

This file contains views joined with click. It contains 3 columns:

* Id - view id
* Logtime - view logtime
* ClickId - click id

## ViewableViews.csv

This file contains views joined with viewable view events. It contains 3 columns:

* Id - view id
* Logtime - view logtime
* EventId - viewable view event id

## statistics.csv

This file contains aggregated information about campaign:

* CampaignId
* Views
* Clicks
* ViewableViews
* Rate
