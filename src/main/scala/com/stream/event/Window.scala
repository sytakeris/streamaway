package com.stream.event

import java.time.{Duration, Instant}

import scala.collection.mutable

case class WindowSelector(windowLength: Duration, slideStep: Duration, watermark: Duration = Duration.ZERO)

object Window {
  def fromEvent[T <: Event](windowSelector: WindowSelector, event: T): Window[T] = {
    val startTime = event.logTime
    val window = new Window[T](startTime, startTime.plus(windowSelector.windowLength), windowSelector)
    window.add(event)
    window
  }
}

class Window[T <: Event](val start: Instant, val end: Instant, val windowSelector: WindowSelector) extends Ordered[Window[T]] with Iterable[T] {
  private val data: mutable.Set[T] = mutable.Set()

  def add(event: T): Unit = data.add(event)

  def doesEventBelong(event: T): Boolean = {
    val logTime = event.logTime
    val isEqualOrAfterStart = logTime.equals(this.start) || logTime.isAfter(this.start)
    val isEqualOrBeforeEnd  = logTime.equals(this.end) || logTime.isBefore(this.end)

    isEqualOrAfterStart && isEqualOrBeforeEnd
  }

  def slideWindow(event: T): Option[Window[T]] = {
    val slideStep = windowSelector.slideStep
    val logTime = event.logTime
    val isEqualOrAfterStart = logTime.equals(this.start.plus(slideStep)) || logTime.isAfter(this.start.plus(slideStep))
    val isEqualOrBeforeEnd  = logTime.equals(this.end.plus(slideStep)) || logTime.isBefore(this.end.plus(slideStep))

    if (isEqualOrAfterStart && isEqualOrBeforeEnd) {
      Some(new Window(this.start.plus(slideStep), this.end.plus(slideStep), this.windowSelector))
    } else {
      None
    }

  }

  override def compare(that: Window[T]): Int = {
    if (this.start.equals(that.start) && this.end.equals(that.end)) {
      0
    } else {
      this.start.compareTo(that.start)
    }
  }

  override def iterator: Iterator[T] = data.iterator
}