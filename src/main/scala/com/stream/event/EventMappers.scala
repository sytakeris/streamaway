package com.stream.event

import java.time.{Instant, ZoneId}
import java.time.format.DateTimeFormatter

object EventMappers {
  def viewMapper(line: String): View = {
    val Array(id, logTime, campaignId) = line.split(",")

    View(id, Instant.from(EventDateFormatter.parse(logTime)), campaignId)
  }

  def clickMapper(line: String): Click = {
    val Array(id, logTime, campaignId, interactionId) = line.split(",")

    Click(id, Instant.from(EventDateFormatter.parse(logTime)), campaignId, interactionId)
  }

  def viewableViewEventsMapper(line: String): ViewableViewEvent = {
    val Array(id, logTime, interactionId) = line.split(",")

    ViewableViewEvent(id, Instant.from(EventDateFormatter.parse(logTime)), interactionId)
  }

  def viewWithClicksMapper(line: String): ViewsWithClick = {
    val Array(id, logTime, clickId) = line.split(",")

    ViewsWithClick(id, Instant.from(EventDateFormatter.parse(logTime)), clickId)
  }

  def viewableViewMapper(line: String): ViewableView = {
    val Array(id, logTime, eventId) = line.split(",")

    ViewableView(id, Instant.from(EventDateFormatter.parse(logTime)), eventId)
  }

}
