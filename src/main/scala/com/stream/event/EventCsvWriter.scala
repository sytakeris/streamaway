package com.stream.event

import java.io.Writer

import scala.jdk.CollectionConverters._
import com.google.common.hash.{BloomFilter, Funnels}
import com.opencsv.{CSVWriter, ICSVWriter}

class EventCsvWriter(writer: Writer, headers: Array[String]) {
  private val write = new CSVWriter(
    writer,
    ICSVWriter.DEFAULT_SEPARATOR,
    ICSVWriter.NO_QUOTE_CHARACTER,
    ICSVWriter.DEFAULT_ESCAPE_CHARACTER,
    ICSVWriter.DEFAULT_LINE_END)
  private val filter: BloomFilter[String] = BloomFilter.create(Funnels.unencodedCharsFunnel(), 10000, 0.01)
  write.writeNext(headers)

  def write(data: List[Array[String]]): Unit = {
    val dedupData = data.filterNot(item => filter.mightContain(item.mkString))
    write.writeAll(dedupData.asJava)
    dedupData.foreach(item => filter.put(item.mkString))
  }

  def close(): Unit = write.close()

}
