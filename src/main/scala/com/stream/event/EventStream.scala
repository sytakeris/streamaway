package com.stream.event


import scala.collection.mutable
import scala.io.Source

class EventStream[T <: Event](source: Source, mapper: String => T,  windowSelector: WindowSelector) extends Iterable[Window[T]] {
  override def iterator: Iterator[Window[T]] = new Iterator[Window[T]] {
    private val csvIterator = source.getLines().drop(1)
    private val openWindows: mutable.Set[Window[T]] = createWindowSet()

    private def createWindowSet(): mutable.TreeSet[Window[T]] = {
      val windowSet = mutable.TreeSet[Window[T]]()
      if (csvIterator.hasNext) {
        val event = mapper(csvIterator.next())
        val window = Window.fromEvent(windowSelector, event)
        windowSet.add(window)
      }

      windowSet
    }

    private def popWindow(): Window[T] = {
      val head = openWindows.head
      openWindows.remove(head)
      head
    }

    override def hasNext: Boolean = csvIterator.hasNext || openWindows.nonEmpty

    override def next(): Window[T] = {
      if (!csvIterator.hasNext) {
        return popWindow()
      }

      for (line <- csvIterator) {
        val event = mapper(line)

        if (openWindows.isEmpty) {
          openWindows.add(Window.fromEvent(windowSelector, event))
        }

        openWindows.foreach {window =>
          if (window.doesEventBelong(event)) {
            window.add(event)
          }
        }

        val slidedWindow = openWindows.last.slideWindow(event)
        if (slidedWindow.nonEmpty) {
          slidedWindow.get.add(event)
          openWindows.add(slidedWindow.get)
        }

        val closedWindow = openWindows.find(_.end.isBefore(event.logTime.minus(windowSelector.watermark)))

        if (closedWindow.nonEmpty) {
          openWindows.remove(closedWindow.get)
          return closedWindow.get
        }
      }

      popWindow()
    }
  }

  def join[B <: Event](stream: EventStream[B], keySelectorA: T => String ,keySelectorB: B => String)(onJoin: List[(T, B)] => Unit): Unit = {
    this.zip(stream).foreach({case (firstWindow, secondWindow) =>
      val eventMap = secondWindow.foldLeft(Map[String, B]()) { (acc, item) =>
        acc.updated(keySelectorB(item), item)
      }

      val joinedData = firstWindow.foldLeft(List[(T, B)]()) { (acc, item) =>
        eventMap.get(keySelectorA(item)) match {
          case Some(secondWindowItem) => {
            acc.appended((item, secondWindowItem))
          }
          case None => acc
        }
      }

      onJoin(joinedData)
    })
  }
}

