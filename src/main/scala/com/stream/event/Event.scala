package com.stream.event

import java.time.Instant

sealed trait Event {
  val logTime: Instant
}

case class View(id: String, logTime: Instant, campaignId: String) extends Event
case class Click(id: String, logTime: Instant, campaignId: String, interactionId: String) extends Event
case class ViewableViewEvent(id: String, logTime: Instant, interactionId: String) extends Event
case class ViewableView(id: String, logTime: Instant, eventId: String) extends Event
case class ViewsWithClick(id: String, logTime: Instant, clickId: String) extends Event
