package com.stream.event

import java.time.{Instant, ZoneId}
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

object EventDateFormatter {
  val formatter: DateTimeFormatter = DateTimeFormatter
    .ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
    .withZone(ZoneId.of("UTC"))

  def parse(value: String): TemporalAccessor = formatter.parse(value)
  def format(value: Instant): String = formatter.format(value)

}
