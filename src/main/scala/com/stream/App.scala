package com.stream

import java.io.FileWriter
import java.time.format.DateTimeFormatter
import java.time.{Duration, ZoneId}

import com.stream.event.{Click, EventCsvWriter, EventDateFormatter, EventMappers, EventStream, ViewableView, ViewableViewEvent, ViewsWithClick, WindowSelector}

import scala.collection.mutable
import scala.io.Source


object App {
  val VIEWS_WITH_CLICKS = "ViewsWithClicks.csv"
  val VIEWABLE_VIEWS = "ViewableViews.csv"
  val STATISTICS = "statistics.csv"

  def main(args: Array[String]): Unit = {
    val currentDir = System.getProperty("user.dir")
    val Array(views, clicks, viewableViews) = args
    joinViewWithClick(currentDir, views, clicks)
    joinViewWithViewableEvents(currentDir, views, viewableViews)
    aggregateData(currentDir, views)
  }

  def joinViewWithClick(currentDir: String, views: String, clicks: String): Unit = {
    val windowSelector = WindowSelector(
      Duration.ofHours(5),
      Duration.ofMinutes(5),
      Duration.ofSeconds(5))

    val clickSelector = WindowSelector(
      Duration.ofHours(5),
      Duration.ofMillis(500),
      Duration.ofHours(1))

    val viewStream = new EventStream(
      Source.fromFile(s"$currentDir/$views"),
      EventMappers.viewMapper,
      windowSelector)


    val clicksStream = new EventStream(
      Source.fromFile(s"$currentDir/$clicks"),
      EventMappers.clickMapper,
      clickSelector)


    val viewWithClicksCsvWriter = new EventCsvWriter(
      new FileWriter(s"${currentDir}/${VIEWS_WITH_CLICKS}"),
      Array("Id", "Logtime", "ClickId"))
    viewStream.join[Click](clicksStream, _.id, _.interactionId) { joinedData =>
      val csv = joinedData.map { case (view, click) =>
        Array(view.id, EventDateFormatter.format(view.logTime), click.id)
      }

      viewWithClicksCsvWriter.write(csv)
    }

    viewWithClicksCsvWriter.close()
  }

  def joinViewWithViewableEvents(currentDir: String, views: String, viewableViews: String): Unit = {
    val windowSelector = WindowSelector(
      Duration.ofHours(1),
      Duration.ofMinutes(5),
      Duration.ofSeconds(5))

    val viewStream = new EventStream(
      Source.fromFile(s"$currentDir/$views"),
      EventMappers.viewMapper,
      windowSelector)

    val viewableViewEventStream = new EventStream(
      Source.fromFile(s"$currentDir/$viewableViews"),
      EventMappers.viewableViewEventsMapper,
      windowSelector)

    val viewableViewWriter = new EventCsvWriter(
      new FileWriter(s"${currentDir}/${VIEWABLE_VIEWS}"),
      Array("Id", "Logtime", "EventId"))
    viewStream.join[ViewableViewEvent](viewableViewEventStream, _.id, _.interactionId) { joinedData =>
      val csv = joinedData.map { case (view, viewEvent) =>
        Array(view.id, EventDateFormatter.format(view.logTime), viewEvent.id)
      }

      viewableViewWriter.write(csv)
    }

    viewableViewWriter.close()
  }

  def aggregateData(currentDir: String, view: String): Unit = {
    val viewsWithClicksMap = mutable.Map[String, ViewsWithClick]()
    val viewableViewsMap = mutable.Map[String, ViewableView]()


     Source.fromFile(s"$currentDir/${VIEWS_WITH_CLICKS}").getLines().drop(1).foreach { line =>
      val event = EventMappers.viewWithClicksMapper(line)
      viewsWithClicksMap.put(event.id, event)
    }
     Source.fromFile(s"$currentDir/${VIEWABLE_VIEWS}").getLines().drop(1).foreach { line =>
      val event = EventMappers.viewableViewMapper(line)
      viewableViewsMap.put(event.id, event)
    }

    type ViewCount = Int
    type ClickCount = Int
    type ViewableViewCount = Int

    val aggregationState = mutable.Map[String, (ViewCount, ClickCount, ViewableViewCount)]()

    Source.fromFile(s"$currentDir/$view").getLines().drop(1)
      .foreach { line =>
        val event = EventMappers.viewMapper(line)
        val state = aggregationState.getOrElse(event.campaignId, (0, 0, 0))
        val viewCount = state._1 + 1
        val clickCount = if (viewsWithClicksMap.get(event.id).nonEmpty) {
          state._2 + 1
        } else {
          state._2
        }
        val viewableViewCount = if (viewableViewsMap.get(event.id).nonEmpty) {
          state._3 + 1
        } else {
          state._3
        }
        aggregationState.put(event.campaignId, (viewCount, clickCount, viewableViewCount))
      }

    val statisticsWriter = new EventCsvWriter(
      new FileWriter(s"${currentDir}/${STATISTICS}"),
      Array("CampaignId", "Views", "Clicks", "ViewableViews", "Rate"))

    val csvData = aggregationState.map(pair => {
      val (campaignId, state) = pair
      val (viewCount, clickCount, viewableViewCount) = state
      val clickThroughRate = clickCount.toFloat / viewCount.toFloat * 100

      Array(campaignId, viewCount.toString, clickCount.toString, viewableViewCount.toString, f"$clickThroughRate%.3f")
    }).toList

    statisticsWriter.write(csvData)
    statisticsWriter.close()

  }
}
