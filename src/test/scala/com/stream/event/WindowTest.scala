package com.stream.event

import java.time.{Duration, Instant}

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers._
import org.scalatest.prop.TableDrivenPropertyChecks._


class WindowTest extends AnyFunSpec {

  describe("SlidingWindow") {
    describe("Creating window") {
      val event = View("1", Instant.now(), "1")
      val selector = WindowSelector(Duration.ofMinutes(1), Duration.ofMinutes(1), Duration.ofMinutes(1))


      it("should contain the event the window is based on") {
        val window = Window.fromEvent(selector, event)

        window should have size 1
        window.head should equal(event)
      }

      it("should have correct start and end range") {
        val window = Window.fromEvent(selector, event)

        window.start should equal(event.logTime)
        window.end should equal(event.logTime.plus(selector.windowLength))
      }
    }

    describe("Comparing windows") {
      it("should show that window are equal when start and end matches") {
        val event = View("1", Instant.now(), "1")
        val selector = WindowSelector(Duration.ofMinutes(1), Duration.ofMinutes(1), Duration.ofMinutes(1))

        val window1 = Window.fromEvent(selector, event)
        val window2 = Window.fromEvent(selector, event)

        window1.compare(window2) should be(0)
      }

      it("should not compare") {
        val event = View("1", Instant.now(), "1")
        val selector = WindowSelector(Duration.ofMinutes(1), Duration.ofMinutes(1), Duration.ofMinutes(1))

        val window1 = Window.fromEvent(selector, event)
        val window2 = Window.fromEvent(selector, event)

        window1.compare(window2) should be(0)
      }

      it("should compare only start time when windows are not equal") {
        val event1 = View("1", Instant.ofEpochSecond(100), "1")
        val event2 = View("1", Instant.ofEpochSecond(200), "1")
        val selector1 = WindowSelector(Duration.ofMinutes(1), Duration.ofMinutes(1))
        val selector2 = WindowSelector(Duration.ofSeconds(30), Duration.ofMinutes(1))

        val window1 = Window.fromEvent(selector1, event1)
        val window2 = Window.fromEvent(selector2, event2)

        window1.compare(window2) should be < 0
      }
    }

    describe("Checking if event belongs to window") {
      it("should correctly determine if event belongs to the window") {
        val window = new Window[View](
          Instant.ofEpochSecond(200),
          Instant.ofEpochSecond(500),
          WindowSelector(Duration.ofSeconds(300), Duration.ofSeconds(1))
        )

        val table = Table(
          ("event", "belongsToWindow"),
          (View("1", Instant.ofEpochSecond(100), "1"), false),
          (View("1", Instant.ofEpochSecond(200), "1"), true),
          (View("1", Instant.ofEpochSecond(300), "1"), true),
          (View("1", Instant.ofEpochSecond(500), "1"), true),
          (View("1", Instant.ofEpochSecond(600), "1"), false),
        )

        forAll(table) { (event, belongsToWindow) =>
          window.doesEventBelong(event) should be(belongsToWindow)
        }
      }
    }

    describe("Sliding a window") {
      it("should slide a window when event falls into new window range") {
        val window = new Window[View](
          Instant.ofEpochSecond(200),
          Instant.ofEpochSecond(500),
          WindowSelector(Duration.ofSeconds(300), Duration.ofSeconds(100))
        )

        val table = Table(
          ("event", "hasSlidingWindow"),
          (View("1", Instant.ofEpochSecond(100), "1"), false),
          (View("1", Instant.ofEpochSecond(200), "1"), false),
          (View("1", Instant.ofEpochSecond(300), "1"), true),
          (View("1", Instant.ofEpochSecond(500), "1"), true),
          (View("1", Instant.ofEpochSecond(600), "1"), true),
        )

        forAll(table) { (event, hasSlidingWindow) =>
          window.slideWindow(event).nonEmpty should be(hasSlidingWindow)
        }
      }
    }
  }
}
