package com.stream.event

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers._

class EventMappersTest extends AnyFunSpec {
  describe("Mapping events") {
    it("should map string to View event") {
      val event = EventMappers.viewMapper("3865966959797087513,2018-02-22 23:56:07.182,1020893")
      event.id should be("3865966959797087513")
      event.logTime.toString should be("2018-02-22T23:56:07.182Z")
      event.campaignId should be("1020893")
    }

    it("should map string to Click event") {
      val event = EventMappers.clickMapper("151933584000405128,2018-02-22 23:56:07.182,1232120,6869210482273907075")
      event.id should be("151933584000405128")
      event.logTime.toString should be("2018-02-22T23:56:07.182Z")
      event.campaignId should be("1232120")
      event.interactionId should be("6869210482273907075")
    }

    it("should map string to ViewableViewEvent event") {
      val event = EventMappers.viewableViewEventsMapper("151934019000523418,2018-02-22 23:56:07.182,4200958894823595240")
      event.id should be("151934019000523418")
      event.logTime.toString should be("2018-02-22T23:56:07.182Z")
      event.interactionId should be("4200958894823595240")
    }

    it("should map string to ViewableView event") {
      val event = EventMappers.viewableViewMapper("151934019000523418,2018-02-22 23:56:07.182,4200958894823595240")
      event.id should be("151934019000523418")
      event.logTime.toString should be("2018-02-22T23:56:07.182Z")
      event.eventId should be("4200958894823595240")
    }

    it("should map string to ViewWithClick event") {
      val event = EventMappers.viewWithClicksMapper("151934019000523418,2018-02-22 23:56:07.182,4200958894823595240")
      event.id should be("151934019000523418")
      event.logTime.toString should be("2018-02-22T23:56:07.182Z")
      event.clickId should be("4200958894823595240")
    }
  }
}
