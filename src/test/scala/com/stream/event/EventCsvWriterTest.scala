package com.stream.event

import java.io.StringWriter

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers._

class EventCsvWriterTest extends AnyFunSpec {
  describe("Writing CSV") {
    it("should write data to csv format") {
      val stringWriter = new StringWriter()
      val writer = new EventCsvWriter(stringWriter, Array("id", "name", "city"))
      writer.write(List(Array("1", "John", "London"), Array("2", "Amy", "Paris")))
      writer.close()
      stringWriter.toString should be("id,name,city\n1,John,London\n2,Amy,Paris\n")
    }

    it("should skip duplicated line") {
      val stringWriter = new StringWriter()
      val writer = new EventCsvWriter(stringWriter, Array("id", "name", "city"))
      writer.write(List(Array("1", "John", "London")))
      writer.write(List(Array("1", "John", "London"), Array("2", "Amy", "Paris")))
      writer.close()
      stringWriter.toString should be("id,name,city\n1,John,London\n2,Amy,Paris\n")
    }
  }
}
