package com.stream.event

import java.time.Duration

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers._

import scala.collection.mutable
import scala.io.Source

class EventStreamTest extends AnyFunSpec {
  describe("Streaming events") {
    it("Should return a window with events") {
      val source = Source.fromString(
        """id,logtime,campaignid
          |5767776681449439088,2018-02-22 00:00:00.127,1221633
          |8047815418162719221,2018-02-22 00:00:01.687,1232120
          |3042367786416775494,2018-02-22 00:00:03.255,1188683""".stripMargin)

      val data = new EventStream[View](
        source,
        EventMappers.viewMapper,
        WindowSelector(
          Duration.ofHours(1),
          Duration.ofHours(1),
        )).toList

      data should have size (1)
      data.head should have size (3)
    }

    it("Should return two windows with events") {
      val source = Source.fromString(
        """id,logtime,campaignid
          |5767776681449439088,2018-02-22 00:00:00.127,1221633
          |8047815418162719221,2018-02-22 00:00:01.687,1232120
          |3042367786416775494,2018-02-22 00:00:03.255,1188683""".stripMargin)

      val data = new EventStream[View](
        source,
        EventMappers.viewMapper,
        WindowSelector(
          Duration.ofSeconds(2),
          Duration.ofSeconds(2),
        )).toList

      data should have size (2)
      data.head should have size (2)
      data.last should have size (1)

    }
  }

  describe("Joining stream") {
    it("should return list of joined events") {
      val windowSelector = WindowSelector(
        Duration.ofHours(1),
        Duration.ofHours(1),
      )
      val source1 = Source.fromString(
        """id,logtime,campaignid
          |5767776681449439088,2018-02-22 00:00:00.127,1221633
          |8047815418162719221,2018-02-22 00:00:01.687,1232120
          |3042367786416775494,2018-02-22 00:00:03.255,1188683""".stripMargin)

      val source2 = Source.fromString(
        """id,logtime,campaignid,interactionid
          |151925412000204915,2018-02-22 00:00:00.127,1232120,5767776681449439088
          |151925424000383501,2018-02-22 00:00:01.687,1232120,8047815418162719221
          |151925445000332906,2018-02-22 00:00:03.255,1049355,4447456238837908290""".stripMargin)

      val viewStream = new EventStream[View](source1, EventMappers.viewMapper, windowSelector)
      val clickStream = new EventStream[Click](source2, EventMappers.clickMapper, windowSelector)
      val state = mutable.ListBuffer[(View, Click)]()

      viewStream.join[Click](clickStream, _.id, _.interactionId) { pair =>
        state.addAll(pair)
      }

      state should have size 2
      state.map(pair => (pair._1.id, pair._2.id)) should contain allOf(
        ("5767776681449439088", "151925412000204915"),
        ("8047815418162719221", "151925424000383501"),
      )
    }
  }
}
